//
//  UIButton+Background.swift
//  paywash
//
//  Created by Dmitry Avvakumov on 24.04.2018.
//  Copyright © 2018 East Media Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
  
  func setBackground(color:UIColor, state:UIControl.State) {
    let image = self.imageOfPixelImage(color: color)
    self.setBackgroundImage(image, for: state)
  }
  
  func drawPixelImage(color: UIColor) {
    let rectanglePath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 1, height: 1))
    color.setFill()
    rectanglePath.fill()
  }
  
  func imageOfPixelImage(color: UIColor) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(CGSize(width: 1, height: 1), false, 0.0)
    self.drawPixelImage(color: color)
    
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return image
  }
  
}

extension UIButton {
  
  func setBackgroundCornered(color:UIColor, state:UIControl.State) {
    let radius = self.frame.height / 2.0
    let image = imageOfCorneredImage(color: color)?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: radius, bottom: 0, right: radius), resizingMode: .stretch)
    
    self.setBackgroundImage(image, for: state)
  }
  
  func drawRoundedImage(color: UIColor, radius: CGFloat) {
    let w = 2.0 * radius + 20.0
    let h = 2.0 * radius
    //// Rectangle Drawing
    let rectanglePath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: w, height: h), cornerRadius: radius)
    color.setFill()
    rectanglePath.fill()
  }
  
  func imageOfCorneredImage(color: UIColor) -> UIImage? {
    let radius = self.frame.height / 2.0
    let w = 2.0 * radius + 20.0
    let h = 2.0 * radius
    
    UIGraphicsBeginImageContextWithOptions(CGSize(width: w, height: h), false, 0.0)
    self.drawRoundedImage(color: color, radius: radius)
    
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return image
  }
  
}
