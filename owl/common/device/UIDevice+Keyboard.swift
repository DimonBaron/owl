//
//  UIDevice+Keyboard.swift
//  paywash
//
//  Created by adBODKAt on 21.03.2018.
//  Copyright © 2018 East Media Ltd. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class KeyboardManager {
  
  typealias Changes = (height: CGFloat, duration: Double, curve: Int)
  
  let heightSignal = BehaviorRelay<Changes>(value: (0, 0, 0))
  let bag = DisposeBag()
  var actualHeight: CGFloat = 0.0
  
  init() {
    Observable
      .from([
        NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification)
          .map { (notification) -> Changes in
            let userInfo = notification.userInfo ?? [:]
            let h = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
            let d = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let c = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.intValue ?? 0
            
            return (h, d, c)
        },
        NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification)
          .map { (notification) -> Changes in
            let userInfo = notification.userInfo ?? [:]
            let d = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let c = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.intValue ?? 0
            
            return (0, d, c)
        }
        ])
      .merge().subscribe(onNext: { [weak self] (changes) in
        self?.heightSignal.accept(changes)
        if changes.height > 0.0 {
          self?.actualHeight = changes.height
        }
      }).disposed(by: bag)
  }
  
}

extension UIDevice {
  
  static let kbManager = KeyboardManager()
  
  func keyboardHeight() -> Observable<(height: CGFloat, duration: Double, curve: Int)> {
    return UIDevice.kbManager.heightSignal.asObservable()
  }
  
  func keyboardHeightLastValue() -> CGFloat {
    return UIDevice.kbManager.heightSignal.value.height
  }
  
  func actualKeyboardHeight() -> CGFloat {
    return UIDevice.kbManager.actualHeight
  }
  
}

