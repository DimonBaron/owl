//
//  String+PhoneNumber.swift
//  owl
//
//  Created by Dmitriy Avvakumov on 22/01/2019.
//  Copyright © 2019 Dmitriy Avvakumov. All rights reserved.
//

import Foundation

public extension String {
  
  public func isPhoneNumber() -> Bool {
    if self.count > 10 {
      return true
    } else {
      return false
    }
  }
}
