//
//  ViewController.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright © 2018 Dmitriy Avvakumov. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
  
  let bag = DisposeBag()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
  }
  
  @IBAction func pushWelcome() {
    
    let inputData = WelcomeViewModel.ModuleInputData()
    let input = WelcomeViewModel.ModuleInput()
    
    guard let (viewContoller, output) = WelcomeConfigurator.module(inputData: inputData, moduleInput: input) else {
      return
    }
    
    output.phoneNumber.subscribe(onNext: { [weak self] (number) in
      // get number
      self?.pushVerifyNumber(number)
    }).disposed(by: bag)
    
    self.navigationController?.pushViewController(viewContoller as! UIViewController, animated: true)
  }
  
  func pushVerifyNumber(_ phoneNumber:String) {
    let inputData = VerifyPhoneViewModel.ModuleInputData(phoneNumber:phoneNumber)
    let input = VerifyPhoneViewModel.ModuleInput()
    
    guard let (viewContoller, output) = VerifyPhoneConfigurator.module(inputData: inputData, moduleInput: input) else {
      return
    }
    
//    output.phoneNumber.subscribe(onNext: { (number) in
//      // get number
//
//    }).disposed(by: bag)
    
    self.navigationController?.pushViewController(viewContoller as! UIViewController, animated: true)

  }
  
  
}

