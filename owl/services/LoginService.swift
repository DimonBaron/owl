//
//  LoginService.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright © 2018 Dmitriy Avvakumov. All rights reserved.
//

import Foundation

public protocol LoginServiceProtocol {
  func verifyNumber(_ number: String)
}

public class LoginService: LoginServiceProtocol {
  public func verifyNumber(_ number: String) {
    
  }
}
