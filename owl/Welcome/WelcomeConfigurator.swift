//
//  WelcomeConfigurator.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright (c) 2018 Dmitriy Avvakumov. All rights reserved.
//

import UIKit

class WelcomeConfigurator {
  class func configure(inputData:WelcomeViewModel.ModuleInputData,
                       moduleInput: WelcomeViewModel.ModuleInput? = nil) throws
    -> (viewController: UIViewController, moduleOutput:WelcomeViewModel.ModuleOutput) {
    // View controller
    let viewController = createViewController()
      
    // Dependencies
    let dependencies = try createDependencies()
      
    // View model
    let viewModel = WelcomeViewModel(dependencies: dependencies, moduleInputData: inputData)
    let moduleOutput = viewModel.configureModule(input: moduleInput)
    
    viewController.viewModel = viewModel
      
    return (viewController, moduleOutput)
  }
  
  private class func createViewController() -> WelcomeViewController {
    return WelcomeViewController()
  }
  
  private class func createDependencies() throws -> WelcomeViewModel.InputDependencies {
    let dependencies =
      WelcomeViewModel.InputDependencies(loginService: LoginService())
    return dependencies
  }
 
  static func module(
    inputData: WelcomeViewModel.ModuleInputData,
    moduleInput: WelcomeViewModel.ModuleInput? = nil)
    -> (Presentable, WelcomeViewModel.ModuleOutput)? {
      do {
        let output = try WelcomeConfigurator.configure(inputData: inputData, moduleInput: moduleInput)
        return (output.viewController, output.moduleOutput)
      } catch let err {
        print(err)
        return nil
      }
  }
}
