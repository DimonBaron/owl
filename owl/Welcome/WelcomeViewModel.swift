//
//  WelcomeViewModel.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright (c) 2018 Dmitriy Avvakumov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol WelcomeViewOutput {
  func configure(input: WelcomeViewModel.Input) -> WelcomeViewModel.Output
}

class WelcomeViewModel: RxViewModelType, RxViewModelModuleType, WelcomeViewOutput {
  
  // MARK: In/Out struct
  struct InputDependencies {
    let loginService: LoginServiceProtocol
  }
  
  struct Input {
    let appearState: Observable<ViewAppearState>
    let phoneNumber: Observable<String>
    let sendSignal: Observable<Void>
  }
  
  struct Output {
    let title: Observable<String>
    let sendEnabled: Observable<Bool>
    let kbHeight: Observable<(height: CGFloat, duration: Double, curve: Int)>
    let state: Observable<ModelState>
  }
  
  // MARK: Dependencies
  private let dp: InputDependencies
  private let moduleInputData: ModuleInputData
  
  // MARK: Properties
  private let bag = DisposeBag()
  private let modelState: RxViewModelStateProtocol = RxViewModelState()
  
  // MARK: Observables
  private let title = Observable.just("Welcome")
  private let sendEnabled = PublishSubject<Bool>()
  private let phoneNumberSignal = PublishSubject<String>()
  
  private var phoneNumber: String = ""
  
  // MARK: - initializer
  
  init(dependencies: InputDependencies, moduleInputData: ModuleInputData) {
    self.dp = dependencies
    self.moduleInputData = moduleInputData
  }
  
  // MARK: - WelcomeViewOutput
  
  func configure(input: Input) -> Output {
    // Configure input
    input.appearState.subscribe(onNext: { _ in
      // .didLoad and etc
    }).disposed(by: bag)
    input.phoneNumber.subscribe(onNext: { [weak self] (text) in
      if text.isPhoneNumber() {
        self?.sendEnabled.onNext( true )
      } else {
        self?.sendEnabled.onNext( false )
      }
      self?.phoneNumber = text
    }).disposed(by: bag)
    input.sendSignal.subscribe(onNext: { [weak self] _ in
      self?.sendPhone( self?.phoneNumber ?? "" )
    }).disposed(by: bag)
    
    // Configure output
    return Output(
      title: title.asObservable(),
      sendEnabled: sendEnabled.asObservable(),
      kbHeight: UIDevice.current.keyboardHeight().asObservable(),
      state: modelState.state.asObservable()
    )
  }
  
  // MARK: - Module configuration
  
  func configureModule(input: ModuleInput?) -> ModuleOutput {
    // Configure input signals
    
    // Configure module output
    return ModuleOutput(phoneNumber:phoneNumberSignal.asObservable())
  }
  
  // MARK: - Additional
  
  func sendPhone(_ phoneNumber: String) {
    // validate
    phoneNumberSignal.onNext(phoneNumber)
    
  }

  deinit {
    print("-- WelcomeViewModel dead")
  }
}
