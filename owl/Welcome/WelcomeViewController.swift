//
//  WelcomeViewController.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright (c) 2018 Dmitriy Avvakumov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class WelcomeViewController: UIViewController {
  
  // MARK: - Properties
  
  // Dependencies
  var viewModel: WelcomeViewOutput?
  
  // Public
  var bag = DisposeBag()
  
  // Private
  private let viewAppearState = PublishSubject<ViewAppearState>()
  
  // IBOutlet & UI
  lazy var customView: WelcomeView = {
    let customView = WelcomeView(frame: CGRect(x: 0, y: 0, width: 320, height: 480))
    return customView
  }()
  
  // MARK: - View lifecycle
  override func loadView() {
    self.view = customView
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.configureUI()
    
    do {
      try self.configureRx()
    } catch(let err) {
      print(err)
    }
    viewAppearState.onNext(.didLoad)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewAppearState.onNext(.willAppear)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    viewAppearState.onNext(.didAppear)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    viewAppearState.onNext(.willDisappear)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    viewAppearState.onNext(.didDisappear)
  }
  
  // MARK: - Configuration
  private func configureRx() throws {
    guard let model = viewModel else {
      fatalError("Please, set ViewModel as dependency for Welcome")
    }
    let customView = self.customView
    
    let input = WelcomeViewModel.Input(
      appearState: viewAppearState,
      phoneNumber: customView.textField.rx.text.asObservable().filterNil(),
      sendSignal: customView.sendButton.rx.tap.asObservable())
    let output = model.configure(input: input)
    
    output.title.subscribe(onNext: { [weak self] (title) in
      self?.navigationItem.title = title
    }).disposed(by: bag)
    output.sendEnabled.bind(to: customView.sendButton.rx.isEnabled).disposed(by: bag)
    output.kbHeight.asObservable().skip(1).subscribe(onNext: { (chgs) in
      let curve = UIView.AnimationOptions(rawValue: UInt(chgs.curve))
      customView.toggleKeyboard(chgs.height)
      UIView.animate(withDuration: chgs.duration, delay: 0, options: [.beginFromCurrentState, curve], animations: {
        customView.layoutIfNeeded()
      }, completion: nil)
    }).disposed(by: bag)
  }
  
  private func configureUI() {
    
  }
  
  // MARK: - Additional
  
  deinit {
    print("WelcomeViewController deinit")
  }
}
