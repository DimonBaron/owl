//
//  WelcomeView.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright (c) 2018 Dmitriy Avvakumov. All rights reserved.
//

import UIKit
import SnapKit

public class WelcomeView: UIView {
  
  // MARK:- Metrics
  
  struct Metrics {
    struct PhoneField {
      static let top: CGFloat = 24
      static let width: CGFloat = 200
      static let height: CGFloat = 44
    }
    struct SendButton {
      static let width: CGFloat = 120
      static let height: CGFloat = 44
      static let hPadding: CGFloat = 20
      static let bottom: CGFloat = 20
    }
  }
  
  // MARK:- Outlets
  
  public let textField: UITextField = {
    let view = UITextField()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = UIColor(red: 0.94, green: 0.94, blue: 0.94, alpha: 1.0)
    view.layer.cornerRadius = 4.0
    return view
  }()
  
  public let sendButton: UIButton = {
    let view = UIButton(type: .custom)
    view.setTitle("Далее", for: .normal)
    view.setTitleColor(UIColor.white, for: .normal)
    view.setBackground(color: .blue, state: .normal)
    view.setBackground(color: .lightGray, state: .disabled)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layer.cornerRadius = 6.0
    view.isEnabled = false
    return view
  }()
  
  // MARK:- Constraints
  
  private var bottomConstraint:Constraint?
  
  public override init(frame: CGRect = CGRect.zero) {
    super.init(frame: frame)
    configureView()
    addSubviews()
    makeConstraints()
  }
  
  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  private func configureView() {
    backgroundColor = UIColor.white
  }
  
  private func addSubviews() {
    addSubview(textField)
    addSubview(sendButton)
  }
  
  private func makeConstraints() {
    let safeArea = safeAreaLayoutGuide
    
    textField.snp.makeConstraints { (maker) in
      maker.centerX.equalToSuperview()
      maker.width.equalTo( Metrics.PhoneField.width )
      maker.height.equalTo( Metrics.PhoneField.height )
      maker.top.equalTo(safeArea.snp.top).inset( Metrics.PhoneField.top )
    }
    
    sendButton.snp.makeConstraints { [weak self] (maker) in
      maker.centerX.equalToSuperview()
      self?.bottomConstraint = maker.bottom.equalToSuperview().inset( Metrics.SendButton.bottom ).constraint
      maker.left.right.equalToSuperview().inset( Metrics.SendButton.hPadding )
      maker.width.lessThanOrEqualTo( Metrics.SendButton.width )
      maker.height.equalTo( Metrics.SendButton.height )
    }
  }
  
  public func toggleKeyboard(_ height:CGFloat) {
    if let c = bottomConstraint {
      c.update(inset: (height + Metrics.SendButton.bottom))
    }
  }
  
}
