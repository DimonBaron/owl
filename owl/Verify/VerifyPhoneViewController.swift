//
//  VerifyPhoneViewController.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright (c) 2018 Dmitriy Avvakumov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VerifyPhoneViewController: UIViewController {
  
  // MARK: - Properties
  
  // Dependencies
  var viewModel: VerifyPhoneViewOutput?
  
  // Public
  var bag = DisposeBag()
  
  // Private
  private let viewAppearState = PublishSubject<ViewAppearState>()
  
  // IBOutlet & UI
  lazy var customView: VerifyPhoneView = {
    let customView = VerifyPhoneView(frame: CGRect(x: 0, y: 0, width: 320, height: 480))
    return customView
  }()
  
  // MARK: - View lifecycle
  override func loadView() {
    self.view = customView
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.configureUI()
    
    do {
      try self.configureRx()
    } catch(let err) {
      print(err)
    }
    viewAppearState.onNext(.didLoad)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewAppearState.onNext(.willAppear)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    viewAppearState.onNext(.didAppear)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    viewAppearState.onNext(.willDisappear)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    viewAppearState.onNext(.didDisappear)
  }
  
  // MARK: - Configuration
  private func configureRx() throws {
    guard let model = viewModel else {
      fatalError("Please, set ViewModel as dependency for VerifyPhone")
    }
    
    let input = VerifyPhoneViewModel.Input(appearState: viewAppearState)
    let output = model.configure(input: input)
    
    output.title.subscribe(onNext: { [weak self] str in
      self?.navigationItem.title = str
    }).disposed(by: bag)
  }
  
  private func configureUI() {

  }
  
  // MARK: - Additional
  
  deinit {
    print("VerifyPhoneViewController deinit")
  }
}
