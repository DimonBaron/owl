//
//  VerifyPhoneConfigurator.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright (c) 2018 Dmitriy Avvakumov. All rights reserved.
//

import UIKit

class VerifyPhoneConfigurator {
  class func configure(inputData:VerifyPhoneViewModel.ModuleInputData,
                       moduleInput: VerifyPhoneViewModel.ModuleInput? = nil) throws
    -> (viewController: UIViewController, moduleOutput:VerifyPhoneViewModel.ModuleOutput) {
    // View controller
    let viewController = createViewController()
      
    // Dependencies
    let dependencies = try createDependencies()
      
    // View model
    let viewModel = VerifyPhoneViewModel(dependencies: dependencies, moduleInputData: inputData)
    let moduleOutput = viewModel.configureModule(input: moduleInput)
    
    viewController.viewModel = viewModel
      
    return (viewController, moduleOutput)
  }
  
  private class func createViewController() -> VerifyPhoneViewController {
    return VerifyPhoneViewController()
  }
  
  private class func createDependencies() throws -> VerifyPhoneViewModel.InputDependencies {
    let dependencies =
      VerifyPhoneViewModel.InputDependencies()
    return dependencies
  }
 
  static func module(
    inputData: VerifyPhoneViewModel.ModuleInputData,
    moduleInput: VerifyPhoneViewModel.ModuleInput? = nil)
    -> (Presentable, VerifyPhoneViewModel.ModuleOutput)? {
      do {
        let output = try VerifyPhoneConfigurator.configure(inputData: inputData, moduleInput: moduleInput)
        return (output.viewController, output.moduleOutput)
      } catch let err {
        print(err)
        return nil
      }
  }
}
