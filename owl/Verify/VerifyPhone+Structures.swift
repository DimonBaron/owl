//
//  VerifyPhone+Structures.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright (c) 2018 Dmitriy Avvakumov. All rights reserved.

import Foundation
import RxSwift

extension VerifyPhoneViewModel {
  
  // MARK: - initial module data
  struct ModuleInputData {
    let phoneNumber:String
  }
  
  // MARK: - module input structure
  struct ModuleInput {
    
  }
  
  // MARK: - module output structure
  struct ModuleOutput {
    
  }
  
}
