//
//  VerifyPhoneView.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright (c) 2018 Dmitriy Avvakumov. All rights reserved.
//

import UIKit

class VerifyPhoneView: UIView {
  
  override init(frame: CGRect = CGRect.zero) {
    super.init(frame: frame)
    configureView()
    addSubviews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  private func configureView() {
    backgroundColor = .white
  }
  
  private func addSubviews() {
    
  }
  
  private func makeConstraints() {
    
  }
}
