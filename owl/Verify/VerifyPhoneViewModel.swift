//
//  VerifyPhoneViewModel.swift
//  testRx
//
//  Created by Dmitriy Avvakumov on 01/10/2018.
//  Copyright (c) 2018 Dmitriy Avvakumov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol VerifyPhoneViewOutput {
  func configure(input: VerifyPhoneViewModel.Input) -> VerifyPhoneViewModel.Output
}

class VerifyPhoneViewModel: RxViewModelType, RxViewModelModuleType, VerifyPhoneViewOutput {
  
  // MARK: In/Out struct
  struct InputDependencies {
    
  }
  
  struct Input {
    let appearState: Observable<ViewAppearState>
  }
  
  struct Output {
    let title: Observable<String>
    let state: Observable<ModelState>
  }
  
  // MARK: Dependencies
  private let dp: InputDependencies
  private let moduleInputData: ModuleInputData
  
  // MARK: Properties
  private let bag = DisposeBag()
  private let modelState: RxViewModelStateProtocol = RxViewModelState()
  
  // MARK: Observables
  private let title = Observable.just("VerifyPhone")
  
  // MARK: - initializer
  
  init(dependencies: InputDependencies, moduleInputData: ModuleInputData) {
    self.dp = dependencies
    self.moduleInputData = moduleInputData
    
    
  }
  
  // MARK: - VerifyPhoneViewOutput
  
  func configure(input: Input) -> Output {
    // Configure input
    input.appearState.subscribe(onNext: { state in
      // .didLoad and etc
    }).disposed(by: bag)
    
    // Configure output
    return Output(
      title: title.asObservable(),
      state: modelState.state.asObservable()
    )
  }
  
  // MARK: - Module configuration
  
  func configureModule(input: ModuleInput?) -> ModuleOutput {
    // Configure input signals
    
    // Configure module output
    return ModuleOutput()
  }
  
  // MARK: - Additional

  deinit {
    print("-- VerifyPhoneViewModel dead")
  }
}
